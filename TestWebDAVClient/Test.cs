﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using System.Diagnostics;
using System.Security.Cryptography;
using net.kvdb.webdav;
using System.Net;
namespace TestWebDAVClient
{
    

    class Test
    {
        static AutoResetEvent autoResetEvent;
        static void Main(string[] args)
        {
            //忽略SSL憑證問題
            //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            
            WebDAVClient client = new WebDAVClient();
            autoResetEvent = new AutoResetEvent(false);
            client.Server = "http://172.16.7.100/";
            client.User = "Test";
            client.Pass = "pass";
            //client.BasePath = "/";
            
            /*
            //Create new dir
            client.CreateDirComplete += new CreateDirCompleteDel(c_CreateDirComplete);
            client.CreateDir("NewDir3");
            autoResetEvent.WaitOne();
            */
            
            //List
            client.ListComplete += new ListCompleteDel(c_ListComplete);
            client.List();
            autoResetEvent.WaitOne();
            /*
            //Upload
            client.UploadComplete += new UploadCompleteDel(c_UploadComplete);
            client.Upload(@"c:\pen.jpg","pen.jpg");
            autoResetEvent.WaitOne();
            
            
            //Download
            client.DownloadComplete += new DownloadCompleteDel(c_DownloadComplete);
            client.Download("pen.jpg",@"c:\pen.jpg");
            autoResetEvent.WaitOne();
            
            
            //Delete
            client.DeleteComplete += new DeleteCompleteDel(c_DeleteComplete);
            client.Delete("NewDir1");
            autoResetEvent.WaitOne();
            */
            Console.ReadLine();
        }



        
        static void c_DeleteComplete(int statusCode)
        {
            Console.WriteLine("status code : " + statusCode);
            autoResetEvent.Set();
        }

        static void c_UploadComplete(int statusCode)
        {
            Console.WriteLine("status code : " + statusCode);
            autoResetEvent.Set();
        }

        static void c_CreateDirComplete(int statusCode)
        {
            Console.WriteLine("status code : " + statusCode);
            autoResetEvent.Set();
        }

        static void c_ListComplete(int statusCode,List<FileProperties> output)
        {
            foreach (var temp in output)
            {
                Console.WriteLine(temp.Name);
                Console.WriteLine(temp.ContentType);
                Console.WriteLine(temp.ContentLength);
                Console.WriteLine(temp.CreationDate);
                Console.WriteLine(temp.LastModifiedDate);
                Console.WriteLine("");
            }
            autoResetEvent.Set(); 
        }

        static void c_DownloadComplete(int statusCode)
        {
            Console.WriteLine("status code : "+statusCode);
            autoResetEvent.Set();
        }
    }
}
