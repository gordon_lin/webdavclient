#WebDAV in IIS7 over SSL

##設定

---
*	啟用WebDAV(詳細過程[請參考](http://www.microsoft.com/taiwan/technet/iis/expand/WebDAV.aspx))
	*	站台=>[WebDAV編寫規則]=>[啟用WebDAV]
	*	站台=>[WebDAV編寫規則]=>[新增編寫規則]
	*	站台=>[驗證]=>啟用[Windows驗證]
	*	站台=>[授權規則]=>[新增允許規則]
*	設定SSL
	*	於WebDAV站台上按右鍵=>[編輯繫結]=>[新增]=>新增一個HTTPS繫結
	*	選取WebDAV站台=>[WebDAV編寫規則]=>[WebDAV設定]=>[必須使用SSl存取=true] 

##問題

---
###問題(一):
client端嘗試HTTPS連線時會出現以下錯誤：

	基礎連接已關閉: 無法為 SSL/TLS 安全通道建立信任關係。

###問題(二):

用HTTPS連線上傳檔案超過50KB時出現錯誤代碼：	 

	413 : Request Entity Too Large

###問題(三):
用HTTPS連線上傳檔案超過30MB時出現錯誤代碼：

	404 : File not Found 
		 
##解決方法

---
###問題(一) 兩種方法：	

1.	於Client端加入程式碼，可讓Client端忽略憑證：

		ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };


2.	在IIS中加入加入有效的憑證：	 
在IIS 7新增自我簽署憑證時無法指定被授權者(預設為電腦名稱)，所以用[IIS 6提供的工具](http://cid-3c8d41bb553e84f5.skydrive.live.com/browse.aspx/SelfSSL?authkey=yeHVTUTVzGE%24)新增一個同網域名稱的憑證，將憑證匯入"受信任的根憑證授權單位"中。([流程請參閱](http://www.robbagby.com/iis/self-signed-certificates-on-iis-7-the-easy-way-and-the-most-effective-way/ "請參閱"))

###問題(二)：	

*	由於SSL Preload會用到UploadReadAhead參數，若要上傳的檔案超過預設值(48KB)，IIS伺服器就會回傳413錯誤訊息，用cmd執行下列指令修改UploadReadAhead參數(單位為Bytes)：

		C:\Windows\System32\inetsrv\appcmd.exe set config -section:system.webServer/serverRuntime /uploadreadaheadsize:"2147483647" /commit:apphost	


###問題(三)：		

*	IIS7預設最大檔案上傳限制30MB，用cmd執行下列指令修改maxAllowedContentLength參數(單位為Bytes)：

		C:\Windows\System32\inetsrv\appcmd.exe set config -section:requestFiltering -requestLimits.maxAllowedContentLength:"2147483647"`	 



##References	 

---	 
[Create a Self-Certification on IIS7](http://www.robbagby.com/iis/self-signed-certificates-on-iis-7-the-easy-way-and-the-most-effective-way/ "SelfCertification")

[WebDAV over SSL Win7](http://www.headcrash.us/blog/2010/12/webdav-over-ssl-with-windows-7/ "WebDAV over SSL Win7")


[IIS7 WebDAV SSL](http://studentguru.gr/b/kingherc/archive/2009/11/21/webdav-for-iis-7-on-windows-server-2008-r2.aspx "IIS7 WebDAV SSL")

[Wikipedia HTTPS](http://zh.wikipedia.org/wiki/HTTPS "Wikipedia HTTPS")

[Request Entity Too large 413 error](http://forums.iis.net/t/1169257.aspx "Request Entity Too large 413 error")

[IIS serverRuntime Config](http://www.iis.net/configreference/system.webserver/serverruntime "IIS <serverRuntime> Config")

[IIS7 File Upload Size Limits](http://www.webtrenches.com/post.cfm/iis7-file-upload-size-limits "IIS7 File Upload Size Limits")

[微軟IIS官網WebDAV簡介與設定](http://www.microsoft.com/taiwan/technet/iis/expand/WebDAV.aspx)

