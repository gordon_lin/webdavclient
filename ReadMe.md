#WebDAV Client
##簡介

---
WebDAV是由HTTP延伸出來的通訊協議，讓使用者協同編輯與管理儲存在伺服器上的檔案。
WebDAVClient是一組C#的client，支援以下幾種操作：

1.	取得目錄及檔案資訊
2.	建立資料夾
3.	上傳檔案
4.	下載檔案
5.	刪除檔案及資料夾


##使用說明

---
*	**Delegates** used in **WebDAVClient** Class

		:::C#
		delegate void ListCompleteDel(int statusCode,List<FileProperties> output);
		delegate void CreatDirCompleteDel(int statusCode);
		delegate void UploadCompleteDel(int statusCode);
		delegate void DownloadCompleteDel(int statusCode);
		delegate void DeleteCompleteDel(int statusCode);

*	Class **WebDAVClient**

	* 屬性

			:::C#
			//連線設定參數
			string	Server;	 
			int		Port;			//optional
			string	BasePath;		//optional , "/" by default
			string	User;	    	//optional 
			string	Pass;	    	//optional
			string	Domain;			//optional
			
			//event delegate	    
			event ListCompleteDel 		ListComplete;
			event CreateDirCompleteDel 	CreateDirComplete;
			event UploadCompleteDel		UploadComplete;			 
			event DownloadCompleteDel	DownloadComplete;
			event DeleteCompleteDel 	DeleteComplete;		

	* 方法	 
	
			:::C#
			void List();							//Path: "/",Depth="1" by default				
			void List(string Path);	 				//Depth: "1" by default
			void List(string Path,string Depth);	//Depth: "0" 只列出該目錄資訊,
													//		 "1" 列出該目錄及該目錄所包含深度1的檔案與目錄資訊,
													//		 "infinity" 列出該目錄與該目錄下無限深度的檔案與資料夾的資訊
			void CreateDir(string Path);	 
			void Upload(string LocalPath,string RemotePath);	 	 
			void Download(string RemotePath,string LocalPath);	
			void Delete(string Path); 

*	Class **FileProperties** 

	*	屬性
			
			:::C#
			string		Name;								 
			DateTime	CreationDate;		
			DateTime	LastModifiedDate;	
			string		ContentType;		
			string		ContentLength;		

##範例

---

*	設定連線

		:::C#
		static void Main()
		{
			WebDAVClient client = new WebDAVClient();
			client.Server = "https://www.example.com/";
			client.User = "UserName";
			client.Pass = "Password";
		}

*	同步範例

		:::C#
		static AutoResetEvent autoResetEvent;
		static void Main()
		{
			......
			autoResetEvent = new AutoResetEvent(false);
			//指定List動作完成後的callback function
			client.ListComplete += new ListCompleteDel(c_ListComplete);
			client.List();
			//等待List動作完成
			autoResetEvent.WaitOne();
			......
		}
		
		//建立callback function
		static void c_ListComplete(int statusCode,List<FileProperties> output)
		{
			Console.WriteLine("Content Type: " + output.ContentType);
			......
			//動作完成
			autoResetEvent.Set();
		}

* 非同步範例
	
		:::C#
		static void Main()
		{
			......
			//指定Uplaod動作完成後的callback function
			client.UploadComplete += new UploadCompleteDel(c_UploadComplete);	 
			client.Upload("LocalFilePath","RemoteFilePath");	 
			......
		}
		
		//建立Callback function
		static void c_UploadComplete(int statusCode)	 
		{	 	 	 
			......	 
		}



##IIS WebDAV over SSL

---
請參考"WebDAV\_IIS_SSL.md"文件

[WebDAV_IIS7_SSL.md](WebDAV_IIS7_SSL.md)



 

##參考

---
[C# WebDAVClient github by kvdb](https://github.com/kvdb/WebDAVClient "C# WebDAVClient github by kvdb")

[Installing and Configuring WebDAV on IIS7](http://www.iis.net/learn/install/installing-publishing-technologies/installing-and-configuring-webdav-on-iis "Installing and Configuring WebDAV on IIS7")

[RFC 4918: HTTP Extensions for WebDAV](http://www.webdav.org/specs/rfc4918.html "RFC 4918: HTTP Extensions for WebDAV")
